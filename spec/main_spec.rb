ENV['APP_ENV'] = 'test'

require_relative '../app/main.rb'
require 'json'
require 'rspec'
require 'rack/test'

RSpec.describe 'App' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "health check is alive" do
    get '/health-check'

    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)).to eq({"message"=>"Sinatra", "version"=>"1.0"})
  end

  it "returns all countries" do
    get '/countries'

    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)).to have_attributes(:size => 3)
  end

  it "returns a country" do
    get '/countries/1'

    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)).to eq({
      "code" => "CO",
      "dial_code" => "+57",
      "id" => 1,
      "name" => "Colombia",
    })
  end
end